#ifndef __HUMAN_H__
#define __HUMAN_H__
#include "player.h"

class Human:public Player{
public:
	Human();
	~Human();
	void setPiece();
	void makePayment(int amount, Player *player = 0);
	void getPType(Building *b);
	void save(std::ofstream &file);
	void typePlayer();
	char getAnswer();
        void getMoveType(bool testing);
};

#endif

