#include "slc.h"
#include <iostream>
#include <cstdlib>
using namespace std;

#include "human.h"
#include "computer.h"
#include "timscards.h"
#include "board.h"

SLC::SLC(): Unownable("SLC") {}

SLC::~SLC() {}

void SLC::landOn(Human *h) {
  cout << "You landed on SLC" << endl;
  int num = rand() % 100;
  TimsCards *t = TimsCards::getInstance();
  if (num == 0 && t->cardsLeft() != 0) {
    cout << "You got a Times Roll Up The Rim Cup!" << endl;
    h->getTimsCard();
  }
  else {
    num = rand() % 24;
    if (num <= 2) {
      cout << "You move back 3!" << endl;
      this->board->move(h, -3);
    }
    else if (2<num && num<=6){
      cout << "You move back 2!" << endl;
      this->board->move(h, -2);
    }
    else if (6<num && num<=10){
      cout << "You move back 1!" << endl;
      this->board->move(h, -1);
    }
    else if (10<num && num<=13){
      cout << "You move forward 1!" << endl;
      this->board->move(h, 1);
    }
    else if (13<num && num<=17){
      cout << "You move forward 2!" << endl;
      this->board->move(h, 2);
    }
    else if (17<num && num<=21){
      cout << "You move forward 3!" << endl;
      this->board->move(h, 3);
    }
    else if (num==22){
      int curLocation = h->getCurLocation();
      int move = (curLocation < 10) ? (10-curLocation) : 50-curLocation;
      cout << "You move forward " << move << " to DC Tims Line!" << endl;
      this->board->move(h, move);
      h->goToJail();
    }
    else {
      int curLocation = h->getCurLocation();
      int move = 40-curLocation;
      cout << "You move forward " << move << " to Collect Osap!" << endl;
      this->board->move(h, move);
    }
  }
}

void SLC::landOn(Computer *c) {
  cout << "Computer landed on SLC" << endl;
  int num = rand() % 100;
  TimsCards *t = TimsCards::getInstance();
  if (num == 0 && t->cardsLeft() != 0) {
    cout << "Computer got a Times Roll Up The Rim Cup!" << endl;
    c->getTimsCard();
  }
  else {
    num = rand() % 24;
    if (num <= 2) {
      cout << "Computer moves back 3!" << endl;
      this->board->move(c, -3);
    }
    else if (2<num && num<=6){
      cout << "Computer moves back 2!" << endl;
      this->board->move(c, -2);
    }
    else if (6<num && num<=10){
      cout << "Computer moves back 1!" << endl;
      this->board->move(c, -1);
    }
    else if (10<num && num<=13){
      cout << "Computer moves forward 1!" << endl;
      this->board->move(c, 1);
    }
    else if (13<num && num<=17){
      cout << "Computer moves forward 2!" << endl;
      this->board->move(c, 2);
    }
    else if (17<num && num<=21){
      cout << "Computer moves forward 3!" << endl;
      this->board->move(c, 3);
    }
    else if (num==22){
      int curLocation = c->getCurLocation();
      int move = (curLocation < 10) ? (10-curLocation) : 50-curLocation;
      cout << "Computer moves forward " << move << "!" << endl;
      this->board->move(c, move);
      c->goToJail();
    }
    else {
      int curLocation = c->getCurLocation();
      int move = 40-curLocation;
      cout << "Computer moves forward " << move << "!" << endl;
      this->board->move(c, move);
    }
  }
}
