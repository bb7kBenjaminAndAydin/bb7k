#include "ownable.h"
#include <iostream>
using namespace std;

#include "human.h"
#include "computer.h"

Ownable::Ownable(string name, string uwBlock, int purchaseCost): Building(), uwBlock(uwBlock), numInBlock(0), mortgaged(false), purchaseCost(purchaseCost) {
  this->othersInBlock = new Building*[4];
  this->name = name;
}

Ownable::~Ownable() {
  delete[] this->othersInBlock;
}

void Ownable::addOthersInBlock(Building *b) {
  this->othersInBlock[numInBlock] = b;
  this->numInBlock++;
}
string Ownable::getBlockName() { return uwBlock; }

bool Ownable::ownAllInBlock(Player *p) {
  bool ownAll = true;
  for (int i = 0; i < numInBlock; i++) {
    if (this->othersInBlock[i]->getOwner() != p) {
      ownAll = false;
    }
  }
  return ownAll;
}
