#include "dcTimsLine.h"
#include <iostream>
using namespace std;

#include "human.h"
#include "computer.h"

DCTimsLine::DCTimsLine(): Unownable("DCTimsLine") {}

DCTimsLine::~DCTimsLine() {}

void DCTimsLine::landOn(Human *h) {
  cout << "You landed on DC Tims Line!" << endl;
}

void DCTimsLine::landOn(Computer *c) {
  cout << "Computer landed on DC Tims Line!" << endl;
}
