#ifndef __PLAYER_H__
#define __PLAYER_H__
#include <string>
#include <vector>
#include <fstream>

class Board;
class TimsCards;
class Building;
class Piece;
 
class Player{
	protected:
		Board * board;
		std::string name;
		int curLocation;
		std::vector <Building *>  ownedBuildings;
		int money;
		char piece;
		int ownedTims;
		int turnsInJail;
		int totalAssets;
		Piece * pieceTable;
		TimsCards * timscards;
		bool jail;
		bool isBankrupt;
	//	Window * window;
	public:
		Player();
		void setBoard(Board *b);
		void setName(std::string nom);
		std::string getName() const;
		int getMoney() const;
		char getPiece() const;
		int getOwnedTims() const;
		int getTurnsInJail() const;
		int getAssets() const;
		int getCurLocation() const;
        bool isInJail() const;
		bool checkBankrupt() const;
		void addMoney(int amount);
		virtual void setPiece()=0;
		void changeCurLocation(int loc);
		virtual void makePayment(int amount, Player *player = 0)=0;		
		void addBuilding(Building * b);
		void acceptPayment(int amount);
		Building * removeBuilding(std::string nm);
		void getTimsCard();
		virtual void getPType(Building *b) = 0;
		void goToJail();
        void leaveJail();
        void addTurnInJail();
		void changeAssets(int amount);
        void printAssets();
        void useTimsCard();
        void setMoney(int m);
        void setTurnsInJail(int t);
		virtual void save(std::ofstream &file)=0;
		virtual void typePlayer()= 0;
		virtual char getAnswer() = 0;
        virtual void getMoveType(bool testing) = 0;
		virtual ~Player()=0;
};
#endif
