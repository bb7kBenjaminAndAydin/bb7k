#include "gyms.h"
#include <iostream>
#include <cstdlib>
using namespace std;

#include "human.h"
#include "computer.h"

Gyms::Gyms(string name): Ownable(name, "Gyms", 150){}

Gyms::~Gyms(){}

void Gyms::landOn(Computer *c){
  cout << "You landed on " << this->name << endl;
  if (this->whoOwns==0){
    if (c->getMoney() > this->purchaseCost) {
      c->addBuilding(this);
      this->whoOwns = c;
      c->changeAssets(this->purchaseCost);
      c->makePayment(this->purchaseCost);
    }
  }
  else if (c != this->whoOwns && !(this->mortgaged)){
    bool allOwned = true;
    for (int i =0; i < this->numInBlock; i++){
      if (this->getOwner() != othersInBlock[i]->getOwner()) {
        allOwned = false;
      }
    }
    int roll1 = rand() % 6 + 1;
    int roll2 = rand() % 6 + 1;
    cout << "You rolled " << roll1 << " and " << roll2 <<endl;
    if (allOwned) {
      cout << "Charging: " << (roll1 + roll2) * 10 << endl;
      c->makePayment(((roll1 + roll2) * 10), this->whoOwns);
    }
    else {
      cout << "Charging: " << (roll1 + roll2) * 4 << endl;
      c->makePayment(((roll1 + roll2) * 4), this->whoOwns);
    }
  }
  else {}
}

void Gyms::landOn(Human *h){
  cout << "You landed on " << this->name << endl;
  if (this->whoOwns==0){
    string likeToPurchase;
    cout << this->name << " is not purchased. Would you like to purchase it? (y/n)" << endl;
    cout << this->name << " costs: " << this->purchaseCost << endl;
    while(cin >> likeToPurchase) {
      if(likeToPurchase == "y" || likeToPurchase == "n") {
        break;
      }
      cout << "Please enter y or n" << endl;
    }
    if (likeToPurchase == "y") {
      h->addBuilding(this);
      this->whoOwns = h;
      h->changeAssets(this->purchaseCost);
      h->makePayment(this->purchaseCost);
    }
  }
  else if (h != this->whoOwns && !(this->mortgaged)){
    bool allOwned = true;
    for (int i =0; i < this->numInBlock; i++){
      if (this->getOwner() != othersInBlock[i]->getOwner()) {
        allOwned = false;
      }
    }
    int roll1 = rand() % 6 + 1;
    int roll2 = rand() % 6 + 1;
    cout << "You rolled " << roll1 << " and " << roll2 <<endl;
    if (allOwned) {
      cout << "Charging: " << (roll1 + roll2) * 10 << endl;
      h->makePayment(((roll1 + roll2) * 10), this->whoOwns);
    }
    else {
      cout << "Charging: " << (roll1 + roll2) * 4 << endl;
      h->makePayment(((roll1 + roll2) * 4), this->whoOwns);
    }
  }
  else {}
}

void Gyms::mortgage(Player *p){
  if (this->getOwner() != p) {
    cout << "You do not own " << this->name << "." << endl;
  }
  if (mortgaged) {
    cout << "Already mortgaged" << endl;
  }
  else {
    mortgaged = true;
    p->acceptPayment(this->purchaseCost * 0.5);
  }
}
void Gyms::unMortgage(Player *p){
  if (this->getOwner() != p) {
    cout << "You do not own " << this->name << "." << endl;
  }
  else if (mortgaged) {
    mortgaged = false;
    p->makePayment((this->purchaseCost * 0.5) + (this->purchaseCost * 0.1));
  }
  else {
    cout << "Not mortgaged" << endl;
  }
}
void Gyms::improve(Player *p) {
  cout << "Cannot improve Gyms" << endl;
}
void Gyms::unImprove(Player *p) {
  cout << "Cannot unimprove Gyms" << endl;
}

void Gyms::returnToBoard(){
  this->whoOwns = 0;
  this->mortgaged = false;
}

void Gyms::save(ofstream &file) {
  file << this->name;
  if (this->whoOwns == 0) {
    file << " BANK ";
  }
  else {
    file << " " <<  this->whoOwns->getName() << " ";
  }
  if (this->mortgaged) {
    file << -1;
  }
  else {
    file << 0;
  }
  file << endl;
}

void Gyms::getImprovements() {} 

bool Gyms::isImproved() { return false; }
void Gyms::changeOwner(Player *p){
  if (this->whoOwns != 0) {
    this->whoOwns->changeAssets(-(this->purchaseCost));
  }
  if (p != 0) {
    p->changeAssets(this->purchaseCost);
  }
  this->whoOwns = p;
}

void Gyms::swapOwner(Player *p){
  if (this->whoOwns != 0) {
    this->whoOwns->changeAssets(-(this->purchaseCost));
  }
  if (p != 0) {
    p->changeAssets(this->purchaseCost);
  }
  this->whoOwns = p;
  if (this->mortgaged) {
    p->makePayment(this->purchaseCost * 0.1);
  }
}

void Gyms::load(Player *p, int improvements){
  if (improvements == -1) {
    this->mortgaged = true;
  }
  else {

  }

  this->changeOwner(p);
  p->addBuilding(this);
}
