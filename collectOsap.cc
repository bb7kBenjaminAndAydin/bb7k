#include "collectOsap.h"
#include <iostream>
using namespace std;

#include "human.h"
#include "computer.h"

CollectOsap::CollectOsap(): Unownable("CollectOsap") {}

CollectOsap::~CollectOsap() {}

void CollectOsap::landOn(Human *h) {
  cout << "You landed on Collect Osap!" << endl;
}

void CollectOsap::landOn(Computer *c) {
  cout << "Computer landed on Collect Osap!" << endl;
}
