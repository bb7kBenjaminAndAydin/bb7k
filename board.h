#ifndef __BOARD_H__
#define __BOARD_H__
class Player;
class Human;
class Computer;
class Buildings;
class AcademicBuildings;
class CollectOsap;
class CoopFee;
class DCTimsLine;
class GoToTims;
class GooseNesting;
class Gyms;
class NeedlesHall;
class Residences;
class Slc;
class Tuition;
class Building;
#include <iostream>
#include <vector>

class Board {
    Building **board;
    //std::vector <Player *> players;
    Player **players;
    int doublesInARow;
    int numPlayers;
    bool canRoll;
    void removePlayer(int index);
  public:
    Board(int **tuitionCosts, bool loading = false, std::string load = "");
    ~Board();
    void playGame(bool testing);
    void moveType(Player *p, bool testing);
    void makeMove(Human *p, bool testing);
    void makeMove(Computer *p, bool testing);
    void printTextDisplay();
    void printPlayer(int index);
    void move(Player *p, int i);
    bool trade(Human *foo);
    bool trade(Computer *foo);
    void startTrade(Player *p);
    void save(Player *p);

};

#endif
