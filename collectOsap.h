#ifndef __COLLECTOSAP_H__
#define __COLLECTOSAP_H__
#include "unownable.h"
class CollectOsap: public Unownable {
  public:
    CollectOsap();
    ~CollectOsap();
    void landOn(Human *h);
    void landOn(Computer *c);
};

#endif
