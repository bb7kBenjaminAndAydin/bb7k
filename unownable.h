#ifndef __UNOWNABLE_H__
#define __UNOWNABLE_H__
#include "building.h"


class Unownable: public Building {
  public:
    Unownable(std::string name);
    virtual ~Unownable();
    void virtual landOn(Computer *c) = 0;
    void virtual landOn(Human *h) = 0;
    void mortgage(Player *p);
    void unMortgage(Player *p);
    void improve(Player *p);
    void unImprove(Player *p);
    void addOthersInBlock(Building *b);
    std::string getBlockName();
    void returnToBoard();
    void changeOwner(Player *p);
    void swapOwner(Player *p);
    void save(std::ofstream &file);
    Player *getOwner();
    void getImprovements();
    void load(Player *p, int improvements);
    bool ownAllInBlock(Player *p);
    bool isImproved();
};

#endif 
