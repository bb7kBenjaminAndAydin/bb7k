#ifndef __TIMSCARDS_H__
#define __TIMSCARDS_H__

class TimsCards{
	static TimsCards *instance;
	TimsCards();
	int TotalTimsCards;
	static void cleanup();
public:
	static TimsCards *getInstance();
	int cardsLeft();
	void takeCard();
	void giveCard();
};

#endif