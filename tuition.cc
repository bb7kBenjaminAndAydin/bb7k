#include "tuition.h"
#include <iostream>
#include <cstdlib>
using namespace std;

#include "human.h"
#include "computer.h"

Tuition::Tuition(): Unownable("Tuition") {}

Tuition::~Tuition() {}

void Tuition::landOn(Human *h) {
  cout << "You landed on pay Tuition!" << endl;
  int assets = 0.10 * h->getAssets();
  cout << "Would you like to pay:" << endl;
  cout << "1: $300" << endl;
  cout << "2: 10\% of assets" << endl;
  int i;
  while(!(cin >> i) || i<=0 || i>=3) {
    cout << "Please enter a number between 1 and 2" << endl;
  }
  if (i==1) {
    cout << "You are paying $300" << endl;
    h->makePayment(300);
  }
  else {
    cout << "You are paying $" << assets << endl;
    h->makePayment(assets);
  }
}

void Tuition::landOn(Computer *c) {
  cout << "Computer landed on pay Tuition!" << endl;
  int assets = 0.10 * c->getAssets();
  if (assets > 300){
    cout << "Computer is paying the $300" << endl;
    c->makePayment(300);
  }
  else {
    cout << "Computer is paying 10\% of assets ($" << assets << ")" << endl;
    c->makePayment(assets);
  }
}
