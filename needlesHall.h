#ifndef __NEEDLESHALL_H__
#define __NEEDLESHALL_H__
#include "unownable.h"
class NeedlesHall: public Unownable {
  public:
    NeedlesHall();
    ~NeedlesHall();
    void landOn(Human *h);
    void landOn(Computer *c);
};

#endif
