#ifndef __OWNABLE_H__
#define __OWNABLE_H__
#include "building.h"

class Ownable: public Building {
  protected:
    std::string uwBlock;
    int numInBlock;
    Building **othersInBlock;
    bool mortgaged;
    int purchaseCost;
  public:
    Ownable(std::string name, std::string uwBlock, int purchaseCost);
    virtual ~Ownable();
    void virtual landOn(Computer *c) = 0;
    void virtual landOn(Human *h) = 0;
    void virtual mortgage(Player *p) = 0;
    void virtual unMortgage(Player *p) = 0;
    void virtual improve(Player *p) = 0;
    void virtual unImprove(Player *p) = 0;
    void virtual returnToBoard() = 0;
    void virtual save(std::ofstream &file) = 0;
    void virtual getImprovements() = 0;
    bool virtual isImproved() = 0;
    void virtual changeOwner(Player *p) = 0;
    void virtual swapOwner(Player *p) = 0;
    void virtual load(Player *p, int improvements) = 0;
    void addOthersInBlock(Building *b);
    std::string getBlockName();
    bool ownAllInBlock(Player *p);
};

#endif 
