#include "needlesHall.h"
#include <iostream>
#include <cstdlib>
using namespace std;

#include "human.h"
#include "computer.h"
#include "timscards.h"

NeedlesHall::NeedlesHall(): Unownable("NeedlesHall") {}

NeedlesHall::~NeedlesHall() {}

void NeedlesHall::landOn(Human *h) {
  cout << "You landed on Needles Hall!" << endl;
  int num = rand() % 100;
  TimsCards *t = TimsCards::getInstance();
  if (num == 0 && t->cardsLeft() != 0) {
    cout << "You got a Times Roll Up The Rim Cup!" << endl;
    h->getTimsCard();
  }
  else {
    num = rand() % 19;
    if (num==0) {
      cout << "You pay 200!" << endl;
      h->makePayment(200);
    }
    else if (0<num && num<=2){
      cout << "You pay 100!" << endl;
      h->makePayment(100);
    }
    else if (2<num && num<=5){
      cout << "You pay 50!" << endl;
      h->makePayment(50);
    }
    else if (5<num && num<=11){
      cout << "You gain 25!" << endl;
      h->acceptPayment(25);
    }
    else if (11<num && num<=14){
      cout << "You gain 50!" << endl;
      h->acceptPayment(50);
    }
    else if (14<num && num<=16){
      cout << "You gain 100!" << endl;
      h->acceptPayment(100);
    }
    else {
      cout << "You gain 200 "  << endl;
      h->acceptPayment(200);
    }
  }
}

void NeedlesHall::landOn(Computer *c) {
  cout << "Computer landed on Needles Hall!" << endl;
  int num = rand() % 100;
  TimsCards *t = TimsCards::getInstance();
  if (num == 0 && t->cardsLeft() != 0) {
    cout << "You got a Times Roll Up The Rim Cup!" << endl;
    c->getTimsCard();
  }
  else {
    num = rand() % 19;
    if (num == 0) {
      cout << "Computer pays 200!" << endl;
      c->makePayment(200);
    }
    else if (0<num && num<=2){
      cout << "Computer pays 100!" << endl;
      c->makePayment(100);
    }
    else if (2<num && num<=5){
      cout << "Computer pays 50!" << endl;
      c->makePayment(50);
    }
    else if (5<num && num<=11){
      cout << "Computer gains 25!" << endl;
      c->acceptPayment(25);
    }
    else if (11<num && num<=14){
      cout << "Computer gains 50!" << endl;
      c->acceptPayment(50);
    }
    else if (14<num && num<=16){
      cout << "Computer gains 100!" << endl;
      c->acceptPayment(100);
    }
    else {
      cout << "Computer gains 200 "  << endl;
      c->acceptPayment(200);
    }
  }
}
