#include "board.h"
#include <sstream>
#include <fstream>
#include "player.h"
#include "human.h"
#include "computer.h"
#include "piece.h"
#include "building.h"
#include "academicBuildings.h"
#include "collectOsap.h"
#include "coopFee.h"
#include "dcTimsLine.h"
#include "goToTims.h"
#include "gooseNesting.h"
#include "gyms.h"
#include "residences.h"
#include "needlesHall.h"
#include "ownable.h"
#include "slc.h"
#include "timscards.h"
#include "tuition.h"
#include "unownable.h"

using namespace std;
Board::Board(int **tuitionCosts, bool loading, string load): doublesInARow(0), canRoll(true) {
  this->board = new Building*[40];
  this->board[0] = new CollectOsap();
  this->board[1] = new AcademicBuildings("AL", "Arts1", 40, 50, tuitionCosts[0]);
  this->board[2] = new SLC();
  this->board[3] = new AcademicBuildings("ML", "Arts1", 60, 50, tuitionCosts[1]);
  this->board[4] = new Tuition();
  this->board[5] = new Residences("MKV");
  this->board[6] = new AcademicBuildings("ECH", "Arts2", 100, 50, tuitionCosts[2]);
  this->board[7] = new NeedlesHall();
  this->board[8] = new AcademicBuildings("PAS", "Arts2", 100, 50, tuitionCosts[3]);
  this->board[9] = new AcademicBuildings("HH", "Arts2", 120, 50, tuitionCosts[4]);
  this->board[10] = new DCTimsLine();
  this->board[11] = new AcademicBuildings("RCH", "Eng", 140, 100, tuitionCosts[5]);
  this->board[12] = new Gyms("PAC");
  this->board[13] = new AcademicBuildings("DWE", "Eng", 140, 100, tuitionCosts[6]);
  this->board[14] = new AcademicBuildings("CPH", "Eng", 160, 100, tuitionCosts[7]);
  this->board[15] = new Residences("UWP");
  this->board[16] = new AcademicBuildings("LHI", "Health", 180, 100, tuitionCosts[8]);
  this->board[17] = new SLC();
  this->board[18] = new AcademicBuildings("BMH", "Health", 180, 100, tuitionCosts[9]);
  this->board[19] = new AcademicBuildings("OPT", "Health", 200, 100, tuitionCosts[10]);
  this->board[20] = new GooseNesting();
  this->board[21] = new AcademicBuildings("EV1", "Env", 220, 150, tuitionCosts[11]);
  this->board[22] = new NeedlesHall();
  this->board[23] = new AcademicBuildings("EV2", "Env", 220, 150, tuitionCosts[12]);
  this->board[24] = new AcademicBuildings("EV3", "Env", 240, 150, tuitionCosts[13]);
  this->board[25] = new Residences("V1");
  this->board[26] = new AcademicBuildings("PHYS", "Sci1", 260, 150, tuitionCosts[14]);
  this->board[27] = new AcademicBuildings("B1", "Sci1", 260, 150, tuitionCosts[15]);
  this->board[28] = new Gyms("CIF");
  this->board[29] = new AcademicBuildings("B2", "Sci1", 280, 150, tuitionCosts[16]);
  this->board[30] = new GoToTims();
  this->board[31] = new AcademicBuildings("EIT", "Sci2", 300, 200, tuitionCosts[17]);
  this->board[32] = new AcademicBuildings("ESC", "Sci2", 300, 200, tuitionCosts[18]);
  this->board[33] = new SLC();
  this->board[34] = new AcademicBuildings("C2", "Sci2", 320, 200, tuitionCosts[19]);
  this->board[35] = new Residences("REV");
  this->board[36] = new NeedlesHall();
  this->board[37] = new AcademicBuildings("MC", "Math", 350, 200, tuitionCosts[20]);
  this->board[38] = new CoopFee();
  this->board[39] = new AcademicBuildings("DC", "Math", 400, 200, tuitionCosts[21]);
  
  for (int i = 0; i<40; i++) {
    this->board[i]->setBoard(this);
  }

  for (int i = 0; i<40; i++) {
    for (int j = 0; j<40; j++) {
      if (i != j && board[i]->getBlockName() == board[j]->getBlockName()) {
        board[i]->addOthersInBlock(board[j]);
      }
    }
  }
  const int maxNumPlayers = 6;
  const int minNumPlayers = 2;
  int numPlayers =0;
  int compPlayers = 0;
  string s;

  if(loading){
    ifstream file(load.c_str());
    file>>numPlayers;
    this->numPlayers = numPlayers;
    players = new Player *[numPlayers];
    for(int i = 0; i < numPlayers; ++i){
      char answer = 0;
      string n;
      file>>n;
      while(answer != 'c' or answer != 'h'){
        cout<<"Is " << n << " a computer player (c) or a human player (h)?"<<endl;
        cin >> answer;
        if ( answer == 'c' || answer == 'h') {
          break;
        }
      }
      if(answer == 'c'){
        players[i] = new Computer;
      }
      else{
        players[i] = new Human;
      }
      players[i]->setName(n);
      int m;
      file>>m;
      players[i]->setMoney(m);
      file>>m;
      if(m != 10){
        players[i]->changeCurLocation(m);
      }
      else{
        int k;
        players[i]->changeCurLocation(m);
        file>>k;
        if(k == 1){
          players[i]->goToJail();
          int f;
          file>>f;
          players[i]->setTurnsInJail(f);
        }
      }
      players[i]->setPiece();
    }
    string bName;
    string pName;
    int house;
    for (int i = 0; i<28; i++) {
    file>>bName;
    file>>pName;
    file>>house;
    cout << bName << " " << pName << " " << house << endl;
    if (pName != "BANK") {
      for(int i = 0; i < 40; ++i){
        if(board[i]->getBuildingName()==bName){
          for(int j = 0; j < numPlayers; ++j){
            if(players[j]->getName()== pName){
              cout << "Building: " << board[i]->getBuildingName() << " Player: " << players[j]->getName() << endl;
              board[i]->load(players[j], house);
              }
            }
          }
        }
      }
    }
  }
  else {
    cout<<"There can be "<<minNumPlayers<<" to "<<maxNumPlayers<<" in this game"<<endl;
    cout<<"-----------------------------------------------------"<<endl;
    cout<<"How many Players?"<<endl;
	
    while (cin>>s){
      istringstream ss(s);
      if(ss>>numPlayers){
        if(numPlayers<=maxNumPlayers and numPlayers>=minNumPlayers) {
          this->numPlayers = numPlayers;
          break;
        }
        else cout<<"Please insert valid number of players"<<endl;
        }
      else cout<<"Number of player must be integer from 2 to 6"<<endl;	
    }
	
    cout<<"How many computer players out of "<<numPlayers<<" do you want to play?"<<endl;
	
    while (cin>>s){
      istringstream ss(s);
      if(ss>>compPlayers){
        if(compPlayers<=numPlayers and compPlayers >= 0) break;
        else cout<<"Please insert valid number of computer players"<<endl;
      }
      else cout<<"Number of computer Players must be an integer from 0 to "<<numPlayers<<endl;
    }
  
//  test
    cout<<"Number of Players are "<<numPlayers<<endl;
    cout<<"Number of Computer players are "<<compPlayers<<endl;
    cout<<"Number of human players are "<<numPlayers - compPlayers<<endl;

    this->players = new Player *[numPlayers];
    for(int i = 0; i < numPlayers; ++i){
      this->players[i]=0;
    }
    for(int i = 0; i < numPlayers - compPlayers; ++i){
      this->players[i] = new Human;
      cout<<"What is your name? -- Player #"<<i+1<<endl;
      cin>>s;
      this->players[i]->setName(s);
      this->players[i]->setPiece();
    }
    for(int i = numPlayers-compPlayers ; i < numPlayers; ++i){
      this->players[i] = new Computer;
      cout<<"What is your name? -- CompPlayer #"<<i+1<<endl;
      cin>>s;
      this->players[i]->setName(s);
      this->players[i]->setPiece();
    }
  }
  for(int i = 0; i < numPlayers; ++i){
    players[i]->setBoard(this);
  }
//test
  for(int i = 0; i < numPlayers; ++i){
    cout<<"Player # "<<i+1<<endl;
    cout<<"name is "<<this->players[i]->getName()<<endl;
    cout<<"money is "<<this->players[i]->getMoney()<<endl;
    cout<<"piece is "<<this->players[i]->getPiece()<<endl;
    cout<<"number of tim cards "<<this->players[i]->getOwnedTims()<<endl;
    cout<<"turns in jail "<<this->players[i]->getTurnsInJail()<<endl;
    cout<<"total number of money"<<this->players[i]->getAssets()<<endl;
    cout<<"current location of this player is"<<this->players[i]->getCurLocation()<<endl;
    cout<<"-----------------------------------------------------"<<endl;
  }
}
void Board::move(Player *p, int i) {
  int location = p->getCurLocation();
  location = location + i;
  while (location>=40){
    cout << "You passed collect Osap! Collect $200." << endl;
    p->addMoney(200);
    location-=40;
  }
  while (location<0) {
    location+=40;
  }
  p->changeCurLocation(location);
  board[location]->performAction(p);
}

void Board::playGame(bool testing) {
  int whosTurn = 0;
  while(this->numPlayers > 1) {
    if (whosTurn >= numPlayers) {
      whosTurn = 0;
    }
    this->moveType(this->players[whosTurn], testing);
    if (players[whosTurn]->checkBankrupt()){
      removePlayer(whosTurn);
    }
    else {
      whosTurn++;
    }
  }
  if (this->numPlayers == 1) {
    cout << players[0]->getName() << " won the game!" << endl;
  }
}

void Board::moveType(Player *p, bool testing) {
  p->getMoveType(testing);
}

void Board::removePlayer(int index) {
  numPlayers--;
  Player **temp = new Player *[numPlayers];
  for(int i = 0; i<numPlayers + 1; i++){
    if(i<index) {
      temp[i] = players[i];
    }
    else if (i == index) {

    }
    else{
      temp[i-1] = players[i];
    }
  }
  delete players[index];
  delete[] players;
  players = temp;
}

void Board::makeMove(Computer *p, bool testing) {
  cout << "It is " << p->getName() << "'s turn." << endl;
  cout << p->getName() << " has $" << p->getMoney() << endl;
  cout << endl << endl;
  this->canRoll = true;
  this->doublesInARow = 0;
  this->printTextDisplay();
  while (this->canRoll) { 
    int roll1;
    int roll2;
    if (p->checkBankrupt()) {
      break;
    }
    if(p->isInJail()) {
      if (p->getOwnedTims() > 0) {
        p->useTimsCard();
        p->leaveJail();
      }
      else {
        p->makePayment(50);
        p->leaveJail();
      }
    }
    roll1 = rand() % 6 + 1;
    roll2 = rand() % 6 + 1;
    cout << p->getName() << "'s rolls." << endl;
    cout << "Roll 1: " << roll1 << endl;
    cout << "Roll 2: " << roll2 << endl;
    if (roll1 == roll2) {
      doublesInARow++;
    }
    else {
        this->canRoll = false;
    }
    if (this->doublesInARow == 3) {
      p->goToJail();
      this->printTextDisplay();
      cout << p->getName() << " rolled doubles 3 times in a row! Being sent to the DC Tims Line!" << endl;
      this->canRoll = false;
    }
    else {
      if (p->isInJail() && roll1 == roll2) {
        p->leaveJail();
        cout << p->getName() << " got out of the DC Tims Line!" << endl;
        this->printTextDisplay();
      }
      if (!(p->isInJail())) {
        this->move(p, roll1 + roll2);
        this->printTextDisplay();
      }
      else {
        this->printTextDisplay();
        cout << p->getName() << " didn't get out of the DC Tims Line." << endl;
        p->addTurnInJail();
      }
    }
  }
  int willTrade = rand() % 5;
  if (willTrade == 0) {
    this->startTrade(p);
  }
  for (int i=0; i<40; i++) {
    if (p->getMoney() < 750) break;
    if (board[i]->getOwner() == p && board[i]->ownAllInBlock(p)) {
      board[i]->improve(p);
    }
  }
  for (int i=0; i<40; i++) {
    if (p->getMoney() > 500) break;
    if (board[i]->getOwner() == p && board[i]->isImproved()) {
      board[i]->unImprove(p);
    }
  }
}


void Board::makeMove(Human *p, bool testing) {
  this->canRoll = true;
  this->doublesInARow = 0;
  string s;
  this->printTextDisplay();
  while(true) {
    cout << endl;
    if (p->checkBankrupt()) {
      break;
    }
    cout << "It is " << p->getName() << "'s turn." << endl;
    cout << p->getName() << " has $" << p->getMoney() << endl;
    cout << endl << endl;
    cout << "Please input a command:" << endl;
    cout << "roll" << endl;
    cout << "next" << endl;
    cout << "trade" << endl;
    cout << "improve" << endl;
    cout << "mortgage" << endl;
    cout << "unmortgage" << endl;
    cout << "save" << endl;
    cout << "assets" << endl;
    cin >> s;
    cout << endl << endl << endl;

    if (s == "roll") {
      if (this->canRoll) {
        int roll1 = 0;
        int roll2 = 0;
        char c;
        if(p->isInJail()) {
          while(true) {
            cout << "You are in the DC Tims Line. Would you like to:" << endl;
            cout << "Roll to try and get out (r)" << endl;
            cout << "Make a payment of $50 (p)" << endl;
            if (p->getOwnedTims() > 0) {
              cout << "Use a Tims Cup (t)" << endl;
            }
            cin >> c;
            if (c == 'r' || c == 'p' || (p->getOwnedTims() > 0 && c == 't')) {
              break;
            }
            else {
              cout << "Please input a valid command." << endl;
            }
          }
          if (c == 't') {
            p->useTimsCard();
            p->leaveJail();
          }
          if (c == 'p'){
            p->makePayment(50);
            p->leaveJail();
          }
        }
        if (testing) {
          string s;
          cout << "TESTING MODE! Please input roll 1" << endl; 
          while (cin >> s) {
          istringstream ss(s);
          if (ss >> roll1 and roll1 <=6 and roll1 >=1)break;
          else cout<<"Please insert integer between 1 and 6"<<endl;
          }
          cout << "TESTING MODE! Please input roll 2" << endl; 
          while (cin >> s) {
          istringstream ss(s);
          if (ss >> roll2 and roll2 <=6 and roll2 >=1)break;
          else cout<<"Please insert integer between 1 and 6"<<endl;
          }
        }
        else {
          roll1 = rand() % 6 + 1;
          roll2 = rand() % 6 + 1;
        }
        cout << "Roll 1: " << roll1 << endl;
        cout << "Roll 2: " << roll2 << endl;
        if (roll1 == roll2) {
          doublesInARow++;
        }
        else {
          this->canRoll = false;
        }
        if (this->doublesInARow == 3) {
          p->goToJail();
          this->printTextDisplay();
          cout << "You rolled doubles 3 times in a row! Being sent to the DC Tims Line!" << endl;
          this->canRoll = false;
        }
        else {
          if (p->isInJail() && roll1 == roll2) {
            cout << "You got out of the DC Tims Line" << endl;
            p->leaveJail();
            this->printTextDisplay();
          }
          else if (p->isInJail() && p->getTurnsInJail() == 2) {
            while(true) {
              cout << "You have been in the DC Tims line too long." << endl;
              if (p->getOwnedTims() > 0) {
                cout << "Make a payment of $50 (p)" << endl;
                cout << "Use a Tims Cup (t)" << endl;
                cin >> c;
              }
              else {
                cout << "You do not have any Tims cups! Forced to make a payment." << endl;
                c = 'p';
              }
              if ( c == 'p' || (p->getOwnedTims() > 0 && c == 't')) {
                break;
              }
              else {
                cout << "Please input a valid command." << endl;
              }
            }
            if (c == 't') {
              p->useTimsCard();
            }
            else {
              p->makePayment(50);
            }
            p->leaveJail();
          }
          if (!(p->isInJail())) {
            this->move(p, roll1 + roll2);
            this->printTextDisplay();
          }
          else {
            this->printTextDisplay();
            cout << "You didn't get out of jail." << endl;
            p->addTurnInJail();
          }
        }
      }
      else { 
        this->printTextDisplay();
        cout << "You cannot roll anymore." << endl; 
      } 
    }
    else if (s == "next") {
      if (this->canRoll) {
        this->printTextDisplay();
        cout << "You can still roll!" << endl;
      }
      else {
        cout << endl << endl << endl;
        break;
      }
    }
    else if (s == "assets") {
      this->printTextDisplay();
      p->printAssets();
    }
    else if (s == "trade") {
      startTrade(p);
      this->startTrade(p);
      this->printTextDisplay();
    }
    else if (s == "improve") {
      string toImprove;
      string buyOrSell;
      Building *toChange = 0;
      while (toChange == 0) {
        cout << "What building would you like to improve?" << endl;
        cin >> toImprove;
        for(int i=0; i<40; i++){
          if (board[i]->getBuildingName() == toImprove) {
            toChange = board[i];
            break;
          }
        }
        if (toChange == 0) {
          cout << "Please input a valid building name." << endl;
        }
      }
      cout << "What would you like to do to this property?" << endl;
      cout << "Buy an improvement (buy)" << endl;
      cout << "Sell an improvement (sell)" << endl;
      while(true) {
        cin >> buyOrSell;
        if (buyOrSell == "buy" || buyOrSell == "sell") {
          break;
        }
        else {
          cout << "Please input buy or sell." << endl;
        }
      }
      if (buyOrSell == "buy") {
        toChange->improve(p);
      }
      else {
        toChange->unImprove(p);
      }
      this->printTextDisplay();
    }
    else if (s == "mortgage") {
      Building *toChange = 0;
      string toMortgage;
      while (toChange == 0) {
        cout << "What building would you like to mortgage?" << endl;
        cin >> toMortgage;
        for(int i=0; i<40; i++){
          if (board[i]->getBuildingName() == toMortgage) {
            toChange = board[i];
            break;
          }
        }
        if (toChange == 0) {
          cout << "Please input a valid building name." << endl;
        }
        toChange->mortgage(p);
      }
      this->printTextDisplay();
    }
    else if (s == "unmortgage") {
      Building *toChange = 0;
      string toUnmortgage;
      while (toChange == 0) {
        cout << "What building would you like to unmortgage?" << endl;
        cin >> toUnmortgage;
        for(int i=0; i<40; i++){
          if (board[i]->getBuildingName() == toUnmortgage) {
            toChange = board[i];
            break;
          }
        }
        if (toChange == 0) {
          cout << "Please input a valid building name." << endl;
        }
        toChange->unMortgage(p);
      }
      this->printTextDisplay();
    }
    else if (s == "save") {
      this->save(p);
      this->printTextDisplay();
    }
    else if (testing && s == "take") {
      int pay;
      cin >> pay;
      p->makePayment(pay);
      this->printTextDisplay();
    }
    else {
      this->printTextDisplay();
      cout << endl << "Please input a valid command" << endl;
    }
  }
}

Board::~Board(){
  for(int i = 0; i <40; i++){
    delete board[i];
  }
  delete board;
  for(int i = 0; i < numPlayers; ++i){
    delete players[i];
  }
  delete players;
}

void Board::startTrade(Player * p){
  p->typePlayer();
}

void Board::printPlayer(int index){
  string s ="";
  int count = 0;
  for(int i = 0; i < numPlayers; ++i){
    if(players[i]->getCurLocation() == index){
      count++;
      s+=players[i]->getPiece();
    }
  }
  cout<<s;
  for(int a = 0; a < 7 - count; ++a){
    cout<<" ";
  }
}
bool Board::trade(Human * foo){
  string n;
  Player * other;
  while (true){
    cout<<"Name of other player"<<endl;
    cin>>n;
    for(int i = 0; i < numPlayers; i++){
      if(players[i]->getName() == n) {
        other = players[i];
        break;
      }
    }
    if(! other) {
      cout << "Insert correct player name" << endl;
    }
    else {
      break;
    }
  }
  string give;
  string receive;
  cout << "GIVE" << endl;
  cin>>give;
  cout << "RECEIVE" << endl;
  cin>>receive;
  istringstream gg(give);
  istringstream rr(receive);
  int g;
  int r;
  if( gg>>g and rr>>r){
    if(foo->getMoney()< g){
      cout<<"You dont have that amount"<<endl;
      return false;
    }
    if(other->getMoney()< r){
      cout<<other->getName()<<"does not have that amount"<<endl;
      return false;
    }
    cout<<other->getName()<<"- Do you agree?(y/n)"<<endl; 
    char answer = other->getAnswer();
      if(answer == 'y'){
        foo->addMoney(-g);
        other->addMoney(g);
        other->addMoney(-r);
        foo->addMoney(r);
        return true;
      }
      else{
        cout<<other->getName()<<"does not agree"<<endl;
      }
  }
  istringstream gg1(give);
  istringstream rr1(receive);
  if(gg1 >> g and !(rr1>>r)){
    if(foo->getMoney()< g){
      cout<<"You dont have that amount"<<endl;
      return false;
    }
    Building * temp = other->removeBuilding(receive);
    if(! temp){
      cout<<other->getName()<<" does not own "<<receive<<endl;
      return false;
    }
    else{
      cout<<other->getName()<<"- Do you agree?(y/n)"<<endl;
      char answer=other->getAnswer();
      if(answer == 'y'){
        foo->addMoney(-g);
        other->addMoney(g);
        foo->addBuilding(temp);
        temp->changeOwner(foo);
        return true;
      }
      else{
        other->addBuilding(temp);
        cout<<other->getName()<<"does not agree"<<endl;
        return true;
      }
    }
  } 
  istringstream gg2(give);
  istringstream rr2(receive);
  if(!(gg2 >> g) and rr2>>r){
    Building * temp = foo->removeBuilding(give);
    if(other->getMoney() < r){
      cout<<other->getName()<<"does not have enough money"<<endl;
      return false;
    }
    if(! temp){
      cout<<"you dont own "<<give<<endl;
      return false;
    }
    else{
      cout<<other->getName()<<"- Do you agree?(y/n)"<<endl;
      char answer = other->getAnswer();
      if(answer == 'y'){
        other->addBuilding(temp);
        temp->changeOwner(other);
        foo->addMoney(r);
        other->addMoney(-r);
        return true;
      }
      else{
        foo->addBuilding(temp);
        cout<<other->getName()<<"does not agree"<<endl;
        return true;
      }
    }
  }
  else {
    Building *fb = foo->removeBuilding(give);
    Building *ob = other->removeBuilding(receive);
    if(! fb){
      cout<<"you dont own "<<give<<endl;
      return false;
    }
    else if(! ob){
      cout<<other->getName()<<"does not own "<<receive<<endl;
       return false;
    }
    else{
      cout<<other->getName()<<"- Do you agree?(y/n)"<<endl;
      char answer = other->getAnswer();
      if(answer == 'y'){
        foo->addBuilding(ob);
        ob->changeOwner(foo);
        other->addBuilding(fb);
        fb->changeOwner(other);
        return true;
      }
      else{
        foo->addBuilding(fb);
        other->addBuilding(ob);
        cout<<other->getName()<<"does not agree"<<endl;
        return true;
      }
    }
  }
}

bool Board::trade(Computer *foo){
  int tradeAmount = foo->getMoney()/4;
  if(tradeAmount != 0){
    for(int i = 1; i < 40; i++){
      if(board[i]->getOwner()){
        Player *other = board[i]->getOwner();
        if(other and other != foo){
          cout  <<  "Player " << foo->getName() << " would like to trade " << other->getName()<<" "<<tradeAmount<<" for "<<board[i]->getBuildingName()<<endl;
          Building *ob = other->removeBuilding(board[i]->getBuildingName());
          cout<<other->getName()<<"- Do you agree?(y/n)"<<endl;
          char answer = other->getAnswer();
          if(answer == 'y'){
            foo->addBuilding(ob);
            ob->changeOwner(foo);
            other->addMoney(tradeAmount);
            foo->addMoney(-tradeAmount);
            return true;
          }
          else{
            other->addBuilding(ob);
            return true;
          }
        }
      }
    }
    return false;
  }
  else{
    for(int i = 0; i < 40 ; i++){
      if(board[i]->getOwner()){
        Player *comp = board[i]->getOwner();
        if(foo == comp){
          Player *other1 = 0;
          while(!other1 and other1 != foo){
            int choose = rand()% numPlayers;
            other1 = players[choose];
          }
          int tradeAmount = other1->getMoney()/4;
          cout  <<  "Player " << foo->getName() << " would like to trade " << other1->getName()<<" "<<board[i]->getBuildingName()<<" for "<<tradeAmount<<endl;
          Building * tempo = comp->removeBuilding(board[i]->getBuildingName());
          cout<<other1->getName()<<"- Do you agree?(y/n)"<<endl;
          char answer1 = other1->getAnswer();
          if(answer1 == 'y'){
            other1->addBuilding(tempo);
            tempo->changeOwner(other1);
            other1->addMoney(-tradeAmount);
            comp->addMoney(tradeAmount);
            return true;
          }
          else{
            comp->addBuilding(tempo);
            return true;
          }
        }
      }
    }
  }
  return false;
}




void Board::save(Player *p){
  cout<<"File name"<<endl;
  string n;
  cin>>n;
  ofstream file(n.c_str());
  file << this->numPlayers << endl;
  int index = 0;
  for(int i = 0; i < numPlayers; ++i){
    if(p == players[i]){
      index = i;
      break;
    }
  }
  for(int i = index; i < numPlayers; ++i){
    players[i]->save(file);
  }
  for(int i = 0; i < index; ++i){
    players[i]->save(file);
  }
  for(int i = 0; i < 40; ++i){
    board[i]->save(file);
  }
}

void Board::printTextDisplay(){
  //line 1 stats
  for(int i = 0; i < 89; ++i){
    cout<<"_";
  }
  cout<<endl;  
  //line 1 ends
  //line 2 stars
  cout<<"|Goose  |";
  board[21]->getImprovements();
  cout<<"|NEEDLES|";
  board[23]->getImprovements();
  cout<<"|";
  board[24]->getImprovements();
  cout<<"|V1     |";
  board[26]->getImprovements();
  cout<<"|";
  board[27]->getImprovements();
  cout<<"|CIF    |";
  board[29]->getImprovements();
  cout<<"|GO TO  |"<<endl;
  //line 2 ends
  //line 3 starts
  cout<<"|Nesting|-------|HALL   |-------|-------|       |-------|-------|       |-------|TIMS   |"<<endl;
  //line 3 ends
  //line 4 stars
  cout<<"|       |EV1    |       |EV2    |EV3    |       |PHYS   |B1     |       |B2     |       |"<<endl;
  //line 4 ends
  //line 5 starts
  cout<<"|";
  printPlayer(20);
  cout<<"|";
  printPlayer(21);
  cout<<"|";
  printPlayer(22);
  cout<<"|";
  printPlayer(23);
  cout<<"|";
  printPlayer(24);
  cout<<"|";
  printPlayer(25);
  cout<<"|";
  printPlayer(26);
  cout<<"|";
  printPlayer(27);
  cout<<"|";
  printPlayer(28);
  cout<<"|";
  printPlayer(29);
  cout<<"|";
  printPlayer(30);
  cout<<"|"<<endl;
  //line 5 ends
  //line 6 starts
  cout<<"|";
  for(int i =0; i < 11; ++i){
    for(int a = 0; a < 7; ++a){
      cout<<"_";
    }
    cout<<"|";
  }
  cout<<endl;
  //line 6 ends
  //line 7 starts
  cout<<"|";
  board[19]->getImprovements();
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  board[31]->getImprovements();
  cout<<"|"<<endl;
  //line 7 ends
  //line 8 starts
  cout<<"|-------|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|-------|"<<endl;
  //line 8 ends
  //line 9 starts
  cout<<"|OPT    |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|EIT    |"<<endl;
  //line 9 ends
  //line 10 starts
  cout<<"|";
  printPlayer(19);
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  printPlayer(31);
  cout<<"|"<<endl;
  //line 10 ends
  //line 11 starts
  cout<<"|_______|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|_______|"<<endl;
  //line 11 ends
  //line 12 starts
  cout<<"|";
  board[18]->getImprovements();
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  board[32]->getImprovements();
  cout<<"|"<<endl;
  //line 12 ends
  //line 13 starts
  cout<<"|-------|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|-------|"<<endl;
  //line 13 ends
  //line 14 starts
  cout<<"|BMH    |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|ESC    |"<<endl;
  //line 14 ends
  //line 15 starts
  cout<<"|";
  printPlayer(18);
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  printPlayer(32);
  cout<<"|"<<endl;
  //line 15 ends
  //line 16 starts
  cout<<"|_______|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|_______|"<<endl;
  //line 16 ends
  //line 17 starts
  cout<<"|SLC    |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|SLC    |"<<endl;
  //line 17 ends
  //line 18 starts
  cout<<"|       |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|       |"<<endl;
  //line 18 ends
  //line 19 starts
  cout<<"|       |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|       |"<<endl;
  //line 19 ends
  //line 20 starts
  cout<<"|";
  printPlayer(17);
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  printPlayer(33);
  cout<<"|"<<endl;
  //line 20 ends
  //line 21 starts
  cout<<"|_______|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|_______|"<<endl;
  //line 21 ends
  //line 22 starts
  cout<<"|";
  board[16]->getImprovements();
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  board[34]->getImprovements();
  cout<<"|"<<endl;
  //line 22 ends
  //line 23 starts
  cout<<"|-------|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|-------|"<<endl;
  //line 23 ends
  //line 24 starts
  cout<<"|LHI    |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|C2     |"<<endl;
  //line 24 ends
  //line 25 starts
  cout<<"|";
  printPlayer(16);
  cout<<"|";
    for(int i = 0; i < 16; ++i){
      cout<<" ";
    }
    for(int a = 16; a < 56; ++a){
      cout<<"_";
    }
    for(int b = 56; b < 71; ++b){
      cout<<" ";
  }
  cout<<"|";
  printPlayer(34);
  cout<<"|"<<endl;
  //line 25 ends
  //line 26 starts
  cout<<"|_______|";
    for(int i = 0; i < 16; ++i){
      cout<<" ";
    }
    cout<<"|";
    for(int a = 17; a < 55; ++a){
      cout<<" ";
    }
    cout<<"|";
    for(int b = 56; b < 71; ++b){
      cout<<" ";
    }
  cout<<"|_______|"<<endl;
  //line 26 ends
  //line 27 starts
  cout<<"|UWP    |";
    for(int i = 0; i < 16; ++i){
      cout<<" ";
    }
    cout<<"|";
    cout<<"  ###   ###   #####  ###   ###   ###  ";
    cout<<"|";
    for(int b = 56; b < 71; ++b){
      cout<<" ";
    }
  cout<<"|REV    |"<<endl;
  //line 27 ends
  //line 28 starts
  cout<<"|       |";
    for(int i = 0; i < 16; ++i){
      cout<<" ";
    }
    cout<<"|";
    cout<<"  #  #  #  #     #  #   # #   # #   # ";
    cout<<"|";
    for(int b = 56; b < 71; ++b){
      cout<<" ";
    }
  cout<<"|       |"<<endl;
  //line 28 ends
  //line 29 starts
  cout<<"|       |";
    for(int i = 0; i < 16; ++i){
      cout<<" ";
    }
    cout<<"|";
    cout<<"  ####  ####    #   #   # #   # #   # ";
    cout<<"|";
    for(int b = 56; b < 71; ++b){
      cout<<" ";
    } 
  cout<<"|       |"<<endl;
  //line 29 ends
  //line 30 starts
  cout<<"|";
  printPlayer(15);
  cout<<"|";
    for(int i = 0; i < 16; ++i){
      cout<<" ";
    }
    cout<<"|";
    cout<<"  #   # #   #  #    #   # #   # #   # ";
    cout<<"|";
    for(int b = 56; b < 71; ++b){
      cout<<" ";
    }
  cout<<"|";
  printPlayer(35);
  cout<<"|"<<endl;
  //line 30 ends
  //line 31 starts
  cout<<"|_______|";
    for(int i = 0; i < 16; ++i){
      cout<<" ";
    }
    cout<<"|";
    cout<<"  ####  ####  #      ###   ###   ###  ";
    cout<<"|";
    for(int b = 56; b < 71; ++b){
      cout<<" ";
    }
  cout<<"|_______|"<<endl;
  //line 31 ends
  //line 32 starts
  cout<<"|";
  board[14]->getImprovements();
  cout<<"|";
    for(int i = 0; i < 16; ++i){
      cout<<" ";
    }
    cout<<"|";
    for(int a = 17; a < 55; ++a){
      cout<<"_";
    }
    cout<<"|";
    for(int b = 56; b < 71; ++b){
      cout<<" ";
    }
  cout<<"|NEEDLES|"<<endl;
  //line 32 ends
  //line 33 starts
  cout<<"|-------|";
    for(int i = 0;i < 71; ++i){
    cout<<" ";
    }
  cout<<"|HALL   |"<<endl;
  //line 33 ends
  //line 34 starts
  cout<<"|CPH    |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  };
  cout<<"|       |"<<endl;
  //line 34 ends
  //line 35 starts
  cout<<"|";
  printPlayer(14);
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  printPlayer(36);
  cout<<"|"<<endl;
  //line 35 ends;
  //line 36 starts;
  cout<<"|_______|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|_______|"<<endl;
  //line 36 ends
  //line 37 starts
  cout<<"|";
  board[13]->getImprovements();
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  board[37]->getImprovements();
  cout<<"|"<<endl;
  //line 37 ends
  //line 38 starts
  cout<<"|-------|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|-------|"<<endl;
  //line 38 ends
  //line 39 starts
  cout<<"|DWE    |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|MC     |"<<endl;
  //line 39 ends
  //line 40 starts
  cout<<"|";
  printPlayer(13);
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  printPlayer(37);
  cout<<"|"<<endl;
  //line 40 ends
  //line 41 starts
  cout<<"|_______|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|_______|"<<endl;
  //line 41 ends
  //line 42 starts
  cout<<"|PAC    |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|COOP   |"<<endl;
  //line 42 ends
  //line 43 starts
  cout<<"|       |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|FEE    |"<<endl;
  //line 43 ends
  //line 44 starts
  cout<<"|       |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|       |"<<endl;
  //line 44 ends
  //line 45 starts
  cout<<"|";
  printPlayer(12);
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  printPlayer(38);
  cout<<"|"<<endl;
  //line 45 ends
  //line 46 starts
  cout<<"|_______|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|_______|"<<endl;
  //line 46 ends
  //line 47 starts
  cout<<"|";
  board[11]->getImprovements();
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  board[39]->getImprovements();
  cout<<"|"<<endl;
  //line 47 ends
  //line 48 starts
  cout<<"|-------|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|-------|"<<endl;
  //line 48 ends;
  //line 49 starts
  cout<<"|RCH    |";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|DC     |"<<endl;
  //line 49 ends
  //line 50 starts
  cout<<"|";
  printPlayer(11);
  cout<<"|";
  for(int i = 0;i < 71; ++i){
    cout<<" ";
  }
  cout<<"|";
  printPlayer(39);
  cout<<"|"<<endl;
  //line 50 ends
  //line 51 starts
  cout<<"|_______|";
  for(int i = 0;i < 71; ++i){
    cout<<"_";
  }
  cout<<"|_______|"<<endl;
  //line 51 ends
  //line 52 starts
  cout<<"|DC Tims|";
  board[9]->getImprovements();
  cout<<"|";
  board[8]->getImprovements();
  cout<<"|NEEDLES|";
  board[6]->getImprovements();
  cout<<"|MKV    |TUITION|";
  board[3]->getImprovements();
  cout<<"|SLC    |";
  board[1]->getImprovements();
  cout<<"|COLLECT|"<<endl;
  //line 52 ends
  //line 53 starts
  cout<<"|Line   |-------|-------|HALL   |-------|       |       |-------|       |-------|OSAP   |"<<endl;
  //line 53 ends
  //line 54 starts
  cout<<"|       |HH     |PAS    |       |ECH    |       |       |ML     |       |AL     |       |"<<endl;
  //line 54 ends
  //line 55 starts
  cout<<"|";
  printPlayer(10);
  cout<<"|";
  printPlayer(9);
  cout<<"|";
  printPlayer(8);
  cout<<"|";
  printPlayer(7);
  cout<<"|";
  printPlayer(6);
  cout<<"|";
  printPlayer(5);
  cout<<"|";
  printPlayer(4);
  cout<<"|";
  printPlayer(3);
  cout<<"|";
  printPlayer(2);
  cout<<"|";
  printPlayer(1);
  cout<<"|";
  printPlayer(0);
  cout<<"|"<<endl;
  //line 55 ends
  //line 56 starts
  cout<<"|";
  for(int i =0; i < 11; ++i){
    for(int a = 0; a < 7; ++a){
      cout<<"_";
    }
    cout<<"|";
  }
  cout<<endl;
  //line 56 ends
}
