#include "academicBuildings.h"
#include <iostream>
#include <cstdlib>
using namespace std;

#include "human.h"
#include "computer.h"

AcademicBuildings::AcademicBuildings(string name, string academicBlock,int purchaseCost, int improvementCost, int *tuitionPerImprovement): Ownable(name, academicBlock, purchaseCost), improvementCost(improvementCost), tuitionPerImprovement(tuitionPerImprovement), numImprovements(0) {}

AcademicBuildings::~AcademicBuildings(){
  delete this->tuitionPerImprovement;
}

void AcademicBuildings::landOn(Computer *c){
  cout << "You landed on " << this->name << endl;
  if (this->whoOwns==0){
    if (c->getMoney() > this->purchaseCost) {
      c->addBuilding(this);
      this->whoOwns = c;
      c->changeAssets(this->purchaseCost);
      c->makePayment(this->purchaseCost);
    }
  }
  else if (c != this->whoOwns && !(this->mortgaged)){
    bool allOwned = true;
    for (int i =0; i < this->numInBlock; i++){
      if (this->getOwner() != othersInBlock[i]->getOwner()) {
        allOwned = false;
      }
    }
    int rentCharged = tuitionPerImprovement[this->numImprovements];
    if (this->numImprovements == 0 && allOwned) { 
      rentCharged *=2;
    }
    cout << "Charging: " << rentCharged << endl;
    c->makePayment(rentCharged, this->whoOwns);
  }
  else {}
}

void AcademicBuildings::landOn(Human *h){
  cout << "You landed on " << this->name << endl;
  if (this->whoOwns==0){
    string likeToPurchase;
    cout << this->name << " is not purchased. Would you like to purchase it? (y/n)" << endl;
    cout << this->name << " costs: " << this->purchaseCost << endl;
    while(cin >> likeToPurchase) {
      if(likeToPurchase == "y" || likeToPurchase == "n") {
        break;
      }
      cout << "Please enter y or n" << endl;
    }
    if (likeToPurchase == "y") {
      h->addBuilding(this);
      this->whoOwns = h;
      h->changeAssets(this->purchaseCost);
      h->makePayment(this->purchaseCost);
    }
  }
  else if (h != this->whoOwns && !(this->mortgaged)){
    bool allOwned = true;
    for (int i =0; i < this->numInBlock; i++){
      if (this->getOwner() != othersInBlock[i]->getOwner()) {
        allOwned = false;
      }
    }
    int rentCharged = tuitionPerImprovement[this->numImprovements];
    if (this->numImprovements == 0 && allOwned) { 
      rentCharged *=2;
    }
    cout << "Charging: " << rentCharged << endl;
    h->makePayment(rentCharged, this->whoOwns);
  }
  else {}
}

void AcademicBuildings::mortgage(Player *p){
  if (mortgaged) {
    cout << this->name << " is already mortgaged." << endl;
  }
  else if (this->getOwner() != p) {
    cout << "You do not own " << this->name << "." << endl;
  }
  else if (this->numImprovements != 0) {
    cout << "You must sell all improvements before morgaging " << this->name << "." << endl;
  }
  else {
    p->acceptPayment(this->purchaseCost * 0.5);
    mortgaged = true;
  }
}
void AcademicBuildings::unMortgage(Player *p){
  if (this->getOwner() != p) {
    cout << "You do not own " << this->name << "." << endl;
  }
  else if (mortgaged) {
    p->makePayment((this->purchaseCost * 0.5) + (this->purchaseCost * 0.1));
    mortgaged = false;
  }
  else {
    cout << this->name << " is not mortgaged." << endl;
  }
}
void AcademicBuildings::improve(Player *p) {
  bool allOwned = true;
  for (int i =0; i < this->numInBlock; i++){
    if (this->getOwner() != othersInBlock[i]->getOwner() || this->getOwner() == 0) {
      allOwned = false;
    }
  }
  if (this->getOwner() != p) {
    cout << "You do not own " << this->name << "." << endl;
  }
  else if (!allOwned){
    cout << "You cannot improve " << this->name << "." << endl;
    cout << "You do not own all of the buildings in the " << this->uwBlock << " monopoly block." << endl;
  }
  else if (numImprovements<5) { 
    this->numImprovements++;
    p->changeAssets(this->improvementCost);
    p->makePayment(this->improvementCost);
  }
  else {
    cout << this->name << " cannot be improved further." << endl;
  }
}
void AcademicBuildings::unImprove(Player *p) {
  if (this->getOwner() != p) {
    cout << "You do not own " << this->name << "." << endl;
  }
  else if (numImprovements>0) {
    this->numImprovements--;
    p->changeAssets(-(this->improvementCost));
    p->acceptPayment((this->improvementCost) * 0.5);
  }
  else {
    cout << this->name << " cannot be unimproved further." << endl;
  }
}
void AcademicBuildings::returnToBoard(){
  this->whoOwns = 0;
  this->mortgaged = false;
  this->numImprovements = 0;
}

void AcademicBuildings::save(ofstream &file) {
  file << this->name;
  if (this->whoOwns == 0) {
    file << " BANK ";
  }
  else {
    file << " " << this->whoOwns->getName() << " ";
  }
  if (this->mortgaged) {
    file << -1;
  }
  else {
    file << this->numImprovements;
  }
  file << endl;
}

void AcademicBuildings::getImprovements() {
  int temp = this->numImprovements;
  for (int i = 0; i < 7; i++) {
    if (temp>0) {
      cout << "I";
      temp--;
    }
    else {
      cout << " ";
    }
  }
}

bool AcademicBuildings::isImproved() {
  if (this->numImprovements > 0) {
    return true;
  }
  return false;
}
void AcademicBuildings::changeOwner(Player *p){
  if (this->whoOwns != 0) {
    this->whoOwns->changeAssets(-(this->purchaseCost));
    this->whoOwns->changeAssets(-(this->improvementCost * this->numImprovements));
  }
  if (p != 0) {
    p->changeAssets(this->purchaseCost);
    p->changeAssets(this->improvementCost * this->numImprovements);
  }
  this->whoOwns = p;
}
void AcademicBuildings::swapOwner(Player *p){
  if (this->whoOwns != 0) {
    this->whoOwns->changeAssets(-(this->purchaseCost));
    this->whoOwns->changeAssets(-(this->improvementCost * this->numImprovements));
  }
  if (p != 0) {
    p->changeAssets(this->purchaseCost);
    p->changeAssets(this->improvementCost * this->numImprovements);
  }
  this->whoOwns = p;
  if (this->mortgaged) {
    p->makePayment(this->purchaseCost * 0.1);
  }
}

void AcademicBuildings::load(Player *p, int improvements){
  if (improvements == -1) {
    this->mortgaged = true;
  }
  else {
    this->numImprovements = improvements;
  }

  this->changeOwner(p);
  p->addBuilding(this);
}
