#ifndef __SLC_H__
#define __SLC_H__
#include "unownable.h"

class board;

class SLC: public Unownable {
  public:
    SLC();
    ~SLC();
    void landOn(Human *h);
    void landOn(Computer *c);
};

#endif
