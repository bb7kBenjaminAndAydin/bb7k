#ifndef __RESIDENCES_H__
#define __RESIDENCES_H__
#include "ownable.h"

class Residences: public Ownable {
   public:
    Residences(std::string);
    ~Residences();
    void landOn(Computer *c);
    void landOn(Human *h);
    void mortgage(Player *p);
    void unMortgage(Player *p);
    void improve(Player *p);
    void unImprove(Player *p);
    void returnToBoard();
    void save(std::ofstream &file);
    void getImprovements();
    void changeOwner(Player *p);
    void swapOwner(Player *p);
    void load(Player *p, int improvements);
    bool isImproved();
};

#endif
