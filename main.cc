#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <string>
#include "board.h"

using namespace std;

int main( int argc, char *argv[] ){
  srand(time(NULL));
  bool testing = false;
  bool loading = false;
  string loadFile = "";
  for (int i = 0; i<argc; i++) {
    stringstream arg(argv[i]);
    string temp;
    arg >> temp;
    if (temp == "-testing") {
      testing = true;
    }
    else if (temp == "-load") {
      stringstream s(argv[i+1]);
      loading = true;
      s >> loadFile;
      i++;
      cout << "Loading from file " << loadFile << endl;
    }
  }
  int **array = new int*[22];
  for (int i =0; i<22; i++) {
    array[i] = new int[6];
  }
  array[0][0] = 2; array[0][1] = 10; array[0][2] = 30; array[0][3] = 90; array[0][4] = 160; array[0][5] = 250;
  array[1][0] = 4; array[1][1] = 20; array[1][2] = 60; array[1][3] = 180; array[1][4] = 320; array[1][5] = 450;
  array[2][0] = 6; array[2][1] = 30; array[2][2] = 90; array[2][3] = 270; array[2][4] = 400; array[2][5] = 550;
  array[3][0] = 6; array[3][1] = 30; array[3][2] = 90; array[3][3] = 270; array[3][4] = 400; array[3][5] = 550;
  array[4][0] = 8; array[4][1] = 40; array[4][2] = 100; array[4][3] = 300; array[4][4] = 450; array[4][5] = 600;
  array[5][0] = 10; array[5][1] = 50; array[5][2] = 150; array[5][3] = 450; array[5][4] = 625; array[5][5] = 750;
  array[6][0] = 10; array[6][1] = 50; array[6][2] = 150; array[6][3] = 450; array[6][4] = 625; array[6][5] = 750;
  array[7][0] = 12; array[7][1] = 60; array[7][2] = 180; array[7][3] = 500; array[7][4] = 700; array[7][5] = 900;
  array[8][0] = 14; array[8][1] = 70; array[8][2] = 200; array[8][3] = 550; array[8][4] = 750; array[8][5] = 950;
  array[9][0] = 14; array[9][1] = 70; array[9][2] = 200; array[9][3] = 550; array[9][4] = 750; array[9][5] = 950;
  array[10][0] = 16; array[10][1] = 80; array[10][2] = 220; array[10][3] = 600; array[10][4] = 800; array[10][5] = 1000;
  array[11][0] = 18; array[11][1] = 90; array[11][2] = 250; array[11][3] = 700; array[11][4] = 875; array[11][5] = 1050;
  array[12][0] = 18; array[12][1] = 90; array[12][2] = 250; array[12][3] = 700; array[12][4] = 875; array[12][5] = 1050;
  array[13][0] = 20; array[13][1] = 100; array[13][2] = 300; array[13][3] = 750; array[13][4] = 925; array[13][5] = 1100;
  array[14][0] = 22; array[14][1] = 110; array[14][2] = 330; array[14][3] = 800; array[14][4] = 975; array[14][5] = 1150;
  array[15][0] = 22; array[15][1] = 110; array[15][2] = 330; array[15][3] = 800; array[15][4] = 975; array[15][5] = 1150;
  array[16][0] = 24; array[16][1] = 120; array[16][2] = 360; array[16][3] = 850; array[16][4] = 1025; array[16][5] = 1200;
  array[17][0] = 26; array[17][1] = 130; array[17][2] = 390; array[17][3] = 900; array[17][4] = 1100; array[17][5] = 1275;
  array[18][0] = 26; array[18][1] = 130; array[18][2] = 390; array[18][3] = 900; array[18][4] = 1100; array[18][5] = 1275;
  array[19][0] = 28; array[19][1] = 150; array[19][2] = 450; array[19][3] = 1000; array[19][4] = 1200; array[19][5] = 1400;
  array[20][0] = 35; array[20][1] = 175; array[20][2] = 500; array[20][3] = 1100; array[20][4] = 1300; array[20][5] = 1500;
  array[21][0] = 50; array[21][1] = 200; array[21][2] = 600; array[21][3] = 1400; array[21][4] = 1700; array[21][5] = 2000;
  Board *b = new Board(array, loading, loadFile);
  b->playGame(testing);
  delete b;
  delete[] array;
}
