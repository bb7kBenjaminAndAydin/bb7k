#include "player.h"
#include <iostream>
#include "piece.h"
#include "timscards.h"
#include "building.h"
#include "board.h"

using namespace std;



Player::~Player(){
  for (int i = 0; i < ownedTims; i++) {
    this->useTimsCard();
  }
}

Player::Player():board(0), name("no name"), curLocation(0),money(1500), piece('0'), ownedTims(0), turnsInJail(0), totalAssets(1500), jail(false),isBankrupt(false){
	pieceTable = Piece::getInstance();
	timscards = TimsCards::getInstance();
}


void Player::setName(std::string nom){
	name = nom;
}

string Player::getName() const{
	return name;
}

int Player::getMoney() const{
	return money;
}

char Player::getPiece() const{
	return piece;
}

int Player::getOwnedTims() const{
	return ownedTims;
}

int Player::getTurnsInJail() const{
	return turnsInJail;
}

int Player::getAssets() const{
	return totalAssets;
}
bool Player::isInJail() const{
        return jail;
}
bool Player::checkBankrupt() const{
        return isBankrupt;
}
void Player::addMoney(int amount){
	money+= amount;
	totalAssets += amount;
}

void Player::addBuilding(Building *b){
	ownedBuildings.push_back(b);
}

Building * Player::removeBuilding(std::string nm){
	for(std::vector<Building *>::iterator it = ownedBuildings.begin() ; it != ownedBuildings.end(); ++it){
		if((*it)->getBuildingName() == nm){
			Building * temp = *it;
			ownedBuildings.erase(it);
			return temp;
		}
	}
	return 0;
} 

void Player::acceptPayment(int amount){
	money += amount;
    totalAssets += amount;
}


void Player::getTimsCard(){
	++ownedTims;
	timscards->giveCard();
}


void Player::goToJail(){
	curLocation = 10;
	turnsInJail = 0;
	jail = true;
}
void Player::leaveJail(){
	turnsInJail = 0;
	jail = false;
}
void Player::addTurnInJail(){
	turnsInJail++;
}

void Player::changeAssets(int amount){
	totalAssets += amount;
}

int Player::getCurLocation() const{
	return curLocation;
}


void Player::changeCurLocation(int loc){
	curLocation = loc;
}

void Player::printAssets(){
	cout<<"Player name is: "<<name<<endl;
	cout<<"Player balance(money): "<<money<<endl;
	cout<<"Owned buildings by this player listed below:"<<endl;
	for(vector<Building *>::iterator it = ownedBuildings.begin(); it != ownedBuildings.end(); ++it){
		cout<<(*it)->getBuildingName()<<endl;
	}
        cout << "Total assets value is: " << totalAssets << endl;
}

void Player::setBoard(Board *b){
	board = b;
}

void Player::useTimsCard(){
	--ownedTims;
	timscards->takeCard();
}

void Player::setMoney(int m){
	totalAssets = m;
	money = m;
}

void Player::setTurnsInJail(int t){
	turnsInJail = t;
}
