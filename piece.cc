#include "piece.h"
#include <cstdlib>
#include <iostream>

Piece *Piece::instance =0;

Piece *Piece::getInstance(){
	if(!instance){
		instance = new Piece;
		std::atexit(cleanup);
	}
	return instance;
}

Piece::Piece(){
	mymap["Goose"] = 'G';
	mymap["GRT"] = 'B';
	mymap["TMD"] = 'D';
	mymap["Professor"] = 'P';
	mymap["Student"] = 'S';
	mymap["Money"] = '$';
	mymap["Laptop"] = 'L';
	mymap["PinkTie"] = 'T';
}

void Piece::cleanup(){
	delete instance;
}

char Piece::getPiece(std::string key){
	for (std::map<std::string,char>::iterator it=mymap.begin(); it!=mymap.end(); ++it){
		if(it->first == key){
			char temp = it->second;
			it->second = '0';
			return temp;
		}
	}
	return '0';
}

char Piece::getPiece(){
	for (std::map<std::string,char>::iterator it=mymap.begin(); it!=mymap.end(); ++it){
    	if(it->second != '0'){
    		char temp=it->second;
    		it->second= '0';
    		return temp;
    	}
    }
    return '0';
}

void Piece::printTable(){
	for (std::map<std::string,char>::iterator it=mymap.begin(); it!=mymap.end(); ++it){
		if(it->second != '0')std::cout<<it->first<<std::endl;
	}
}
