#include "gooseNesting.h"
#include <iostream>
using namespace std;

#include "human.h"
#include "computer.h"

GooseNesting::GooseNesting(): Unownable("GooseNesting") {}

GooseNesting::~GooseNesting() {}

void GooseNesting::landOn(Human *h) {
  cout << "You landed on Goose Nesting!" << endl;
}

void GooseNesting::landOn(Computer *c) {
  cout << "Computer landed on Goose Nesting!" << endl;
}
