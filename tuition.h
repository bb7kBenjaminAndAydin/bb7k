#ifndef __TUITION_H__
#define __TUITION_H__
#include "unownable.h"
class Tuition: public Unownable {
  public:
    Tuition();
    ~Tuition();
    void landOn(Human *h);
    void landOn(Computer *c);
};

#endif
