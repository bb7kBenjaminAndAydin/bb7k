#ifndef __COOPFEE_H__
#define __COOPFEE_H__
#include "unownable.h"
class CoopFee: public Unownable {
  public:
    CoopFee();
    ~CoopFee();
    void landOn(Human *h);
    void landOn(Computer *c);
};

#endif
