#include "computer.h"
#include "piece.h"
#include "board.h"
#include "building.h"
#include <iostream>
#include "board.h"
#include <cstdlib>

using namespace std;

Computer::Computer():Player(){}

Computer::~Computer(){}

void Computer::setPiece(){
	piece = pieceTable->getPiece();
}

void Computer::makePayment(int amount, Player *player){
	if(amount <= money){
		money -= amount;
                totalAssets -= amount;
	}
	else{
		cout<<"I am coumputer player and I am declaring bankrupcy"<<endl;
		if(player){
			while(ownedBuildings.size() != 0) {
				Building * temp = removeBuilding(ownedBuildings[0]->getBuildingName());
				player->addBuilding(temp);
                                temp->changeOwner(player);
			}
		}
		else{
			for(std::vector<Building *>::iterator it = ownedBuildings.begin(); it != ownedBuildings.end(); ++it){
				(*it)->returnToBoard();
			}
		}
			this->isBankrupt = true;
	}
}

void Computer::getPType(Building *b){
	b->landOn(this);
}

void Computer::save(ofstream &f){
	if(curLocation == 10 and jail == true){
		f<<name<<" "<<money<<" "<<curLocation<<" "<<1<<" "<<turnsInJail<<endl;
	}
	else if(curLocation == 10){
		f<<name<<" "<<money<<" "<<curLocation<<" "<<0<<endl;
	}
	else{
		f<<name<<" "<<money<<" "<<curLocation<<endl;
	}
}

void Computer::typePlayer(){
	board->trade(this);
}

char Computer::getAnswer(){
	int choose = rand()%2;
	if(choose == 1){
           cout << "Computer accepts your trade." << endl; 
           return 'y';
        }
	else {
          cout << "Computer declines your trade." << endl;
          return 'n';
        }
}
void Computer::getMoveType(bool testing) {
  board->makeMove(this, testing);
}
