#ifndef __GOTOTIMS_H__
#define __GOTOTIMS_H__
#include "unownable.h"
class GoToTims: public Unownable {
  public:
    GoToTims();
    ~GoToTims();
    void landOn(Human *h);
    void landOn(Computer *c);
};

#endif
