#include "timscards.h"
#include <cstdlib>

TimsCards * TimsCards::instance = 0;

TimsCards *TimsCards::getInstance(){
	if(!instance){
		instance = new TimsCards;
		std::atexit(cleanup);
	}
	return instance;
}

TimsCards::TimsCards():TotalTimsCards(4){}

void TimsCards::cleanup(){
	delete instance;
}

int TimsCards::cardsLeft(){
	return TotalTimsCards;
}

void TimsCards::takeCard(){
	++TotalTimsCards;
}

void TimsCards::giveCard(){
	--TotalTimsCards;
}
