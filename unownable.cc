#include "unownable.h"
#include <iostream>
using namespace std;

#include "human.h"
#include "computer.h"

Unownable::Unownable(string name): Building() {
  this->name = name;
}

Unownable::~Unownable() {}

void Unownable::mortgage(Player *p) {
  cout << "Cannot mortgage an unownable property" <<  endl;
}
void Unownable::unMortgage(Player *p) {
  cout << "Cannot unmortgage an unownable property" << endl;
}
void Unownable::improve(Player *p) {
  cout << "Cannot improve an unownable property" << endl;
}
void Unownable::unImprove(Player *p) {
  cout << "Cannot unimpove an unownable property" << endl;
}
void Unownable::returnToBoard() {
  cout << "Unownable properties always belong to the board" << endl;
}
void Unownable::changeOwner(Player *p) {
  cout << "Cannot change the owner of an unownable building!" << endl;
}
void Unownable::swapOwner(Player *p) {
  cout << "Cannot change the owner of an unownable building!" << endl;
}

Player *Unownable::getOwner() {
  cerr << "Attempting to get the owner of an unownable class, shouldn't happen" << endl;
  return 0;
}

void Unownable::addOthersInBlock(Building *b) {
}

string Unownable::getBlockName() { return ""; }

void Unownable::save(ofstream &file) {}

void Unownable::getImprovements(){} 

bool Unownable::ownAllInBlock(Player *p) { return false; }

bool Unownable::isImproved() { return false; } 

void Unownable::load(Player *p, int improvements){}
