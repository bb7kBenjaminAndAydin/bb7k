CXX = g++
CXXFLAGS =-Wall -MMD
EXEC = bb7k
OBJECTS = main.o building.o collectOsap.o coopFee.o dcTimsLine.o goToTims.o gooseNesting.o needlesHall.o slc.o tuition.o unownable.o ownable.o gyms.o residences.o academicBuildings.o player.o human.o computer.o piece.o timscards.o board.o 
DEPENDS = ${OBJECTS:.o=.d}

${EXEC}: ${OBJECTS}
	${CXX} ${CXXFLAGS} ${OBJECTS} -o ${EXEC}

-include ${DEPENDS}

.PHONY: clean

clean:
	rm ${OBJECTS} ${EXEC} ${DEPENDS}
