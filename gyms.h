#ifndef __GYMS_H__
#define __GYMS_H__
#include "ownable.h"

class Gyms: public Ownable {
   public:
    Gyms(std::string);
    ~Gyms();
    void landOn(Computer *c);
    void landOn(Human *h);
    void mortgage(Player *p);
    void unMortgage(Player *p);
    void improve(Player *p);
    void unImprove(Player *p);
    void returnToBoard();
    void save(std::ofstream &file);
    void getImprovements();
    void changeOwner(Player *p);
    void swapOwner(Player *p);
    void load(Player *p, int improvements);
    bool isImproved();
};

#endif
