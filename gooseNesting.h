#ifndef __GOOSENESTING_H__
#define __GOOSENESTING_H__
#include "unownable.h"
class GooseNesting: public Unownable {
  public:
    GooseNesting();
    ~GooseNesting();
    void landOn(Human *h);
    void landOn(Computer *c);
};

#endif
