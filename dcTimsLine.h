#ifndef __DCTIMSLINE_H__
#define __DCTIMSLINE_H__
#include "unownable.h"
class DCTimsLine: public Unownable {
  public:
    DCTimsLine();
    ~DCTimsLine();
    void landOn(Human *h);
    void landOn(Computer *c);
};

#endif
