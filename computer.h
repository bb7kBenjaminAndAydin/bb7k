#ifndef __COMPUTER_H__
#define __COMPUTER_H__
#include "player.h"

class Computer:public Player{
public:
	Computer();
	~Computer();
	void setPiece();
	void makePayment(int amount, Player *player = 0);
	void getPType(Building *b);
	void save(std::ofstream &file);
	void typePlayer();
	char getAnswer();
    void getMoveType(bool testing);
};
#endif
