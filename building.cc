#include "building.h"
#include <iostream>
#include "player.h"

using namespace std;

Building::Building(): name(""), whoOwns(0) {}

Building::~Building() {}	

void Building::setName(string newName) {
  this->name = newName;
}
string Building::getBuildingName(){ return this->name; }

void Building::performAction(Player *p) {
  p->getPType(this);
}

Player *Building::getOwner() {
  if(this->whoOwns) {
    return this->whoOwns;
  }
  return 0;
}

void Building::setBoard(Board *b) {
  this->board = b;
}


