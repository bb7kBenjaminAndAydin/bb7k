#include "goToTims.h"
#include <iostream>
#include <cstdlib>
using namespace std;

#include "human.h"
#include "computer.h"

GoToTims::GoToTims(): Unownable("GoToTims") {}

GoToTims::~GoToTims() {}

void GoToTims::landOn(Human *h) {
  cout << "You landed on Go To Tims!" << endl;
  h->goToJail();
}

void GoToTims::landOn(Computer *c) {
  cout << "Computer landed on GoToTims!" << endl;
  c->goToJail();
}
