#ifndef __PIECE_H__
#define __PIECE_H__
#include <map>
#include <string>

class Piece{
	static Piece *instance;
	Piece();
	
	std::map <std::string, char> mymap;

	static void cleanup();
public:
	static Piece *getInstance();
	char getPiece(std::string key);
	char getPiece();
	void printTable();
};

#endif
