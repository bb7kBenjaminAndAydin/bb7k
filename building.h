#ifndef __BUILDINGS_H__
#define __BUILDINGS_H__
#include <string>
#include <iostream>
#include <cstdlib>
#include <fstream>
class Player;
class Board;
class Human;
class Computer;
class TimsCards;
class Building {
  protected:
    std::string name;
    Player *whoOwns;
    Board *board;
  public:
    Building();
    virtual ~Building();
    void setName(std::string newName);
    std::string getBuildingName();
    virtual void getImprovements() = 0;
    void performAction(Player *p);
    virtual void landOn(Human *h) = 0;
    virtual void landOn(Computer *c) = 0;
    virtual void mortgage(Player *p) = 0;
    virtual void unMortgage(Player *p) = 0;
    virtual void improve(Player *p) = 0;
    virtual void unImprove(Player *p) = 0;    
    virtual void addOthersInBlock(Building *b) = 0;
    virtual std::string getBlockName() = 0;
    Player *getOwner();
    void setBoard(Board *b);
    virtual void returnToBoard() = 0;
    virtual void changeOwner(Player *p) = 0;
    virtual void swapOwner(Player *p) = 0;
    virtual void save(std::ofstream &file) = 0;
    virtual bool ownAllInBlock(Player *p) = 0;
    virtual bool isImproved() = 0;
    virtual void load(Player *p, int improvements) = 0;
};

#endif
