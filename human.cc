#include <string>
#include <iostream>
#include "human.h"
#include "board.h"
#include "piece.h"
#include "building.h"
#include "board.h"

using namespace std;

Human::~Human(){}

Human::Human():Player(){}

void Human::setPiece(){
	std::cout<<"Available pieces"<<std::endl;
	pieceTable->printTable();
	std::cout<<"Choose one of the available piece to resemble you during the game"<<std::endl;
	std::string s;
	while(true){
		std::cin>>s;
		piece = pieceTable->getPiece(s);
		if(piece != '0') break; 
		std::cout<<"Please choose only available piece"<<std::endl;
	}
}

void Human::makePayment(int amount, Player *player){
	cout<<"human player is making payment"<<endl;
	while(true){
		if(amount <= money and !player){
			money -= amount;
			totalAssets -= amount;
			cout<<"Payment was succesfull!(to Bank)"<<endl;
			break;
		}
		else if(amount <= money and player){
			money -=amount;
			totalAssets -= amount;
			player->addMoney(amount);
			cout<<"Payment was successfull!(to Player)"<<endl;
			break;
		}
		else{
			cout<<this->name << ", you dont have enough money. Would you like to sell improvements, mortgage property or declare bankruptcy"<<endl;
			cout<<"You owe " << amount << " but only have " << this->money << ". You need " << amount - this->money << " more!" << endl;
			cout<<"To sell improvements: improve <property> sell"<<endl;
			cout<<"Mortgage property: mortgage <property>"<<endl;
			cout<<"Trade Properties: trade" << endl;
 			cout<<"Display assets: assets"<<endl;
			cout<<"bankruptcy: bankrupt"<<endl;
                        
			int change=0;
			string s1;
			string s2;
			string s3;
			cin>>s1;
			if (s1 == "trade") {
				board->trade(this);
				board->printTextDisplay();
  			}
   			else if (s1 == "assets") {
				board->printTextDisplay();
				this->printAssets();
   			}
			else if(s1 == "improve"){
				while(true){
					cin>>s2;
					for(vector<Building *>::iterator it = ownedBuildings.begin(); it != ownedBuildings.end(); ++it){
						if((*it)->getBuildingName() == s2)change =1;
					}
					if(change == 0){
						cout<<"Please insert building name that you own"<<endl;
						continue;
					}
					else{
						change =0;
						break;
					}

				}
				while(true){
					cin>>s3;
					if(s3 == "sell"){
						for(vector<Building *>::iterator it = ownedBuildings.begin(); it != ownedBuildings.end(); ++it){
							if((*it)->getBuildingName() == s2)(*it)->unImprove(this);
						}
       						break;
					}
					else{
						cout<<"You can only sell improvements at this point"<<endl;
						continue;
					}
				}
				board->printTextDisplay();
			}
			else if(s1 == "mortgage"){
				while(true){
					cin>>s2;
					for(vector<Building *>::iterator it = ownedBuildings.begin(); it != ownedBuildings.end(); ++it){
						if((*it)->getBuildingName() == s2)change =1;
					}
					if(change == 1){
						for(vector<Building *>::iterator it = ownedBuildings.begin(); it != ownedBuildings.end(); ++it){
							if((*it)->getBuildingName()==s2)(*it)->mortgage(this);
						}
						change = 0;
						break;
					}
					else{
						cout<<"Please insert building name that you own"<<endl;
					}
				}
				board->printTextDisplay();
			}
			else if(s1 == "bankrupt"){
				if(!player){
					for(std::vector<Building *>::iterator it = ownedBuildings.begin(); it != ownedBuildings.end(); ++it){
					(*it)->returnToBoard();
					}
					this->isBankrupt = true;	
					break;
				}
				else{
					while(ownedBuildings.size() != 0) {
						Building * temp = removeBuilding(ownedBuildings[0]->getBuildingName());
						player->addBuilding(temp);
   						temp->swapOwner(player);
				}
					this->isBankrupt = true;
					break;	
				}
			}
			else{
				cout<<"Insert valid command"<<endl;
			}
		}
	}
}

void Human::getPType(Building *b){
	b->landOn(this);
}

void Human::save(ofstream &f){
	if(curLocation == 10 and jail == true){
		f<<name<<" "<<money<<" "<<curLocation<<" "<<1<<" "<<turnsInJail<<endl;
	}
	else if(curLocation == 10){
		f<<name<<" "<<money<<" "<<curLocation<<" "<<0<<endl;
	}
	else{
		f<<name<<" "<<money<<" "<<curLocation<<endl;
	}
}
void Human::getMoveType(bool testing) {
  board->makeMove(this, testing);
}

void Human::typePlayer(){
	board->trade(this);
}

char Human::getAnswer(){
	char answer;
	while(true){
		cin >> answer;
		if(answer == 'y' or answer == 'n'){
			break;
		}
		else{
			cout<<"TRY AGAIN y or n!!!"<<endl;
		}
	}
	return answer;
}



