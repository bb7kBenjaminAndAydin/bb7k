#include "coopFee.h"
#include <iostream>
using namespace std;

#include "human.h"
#include "computer.h"

CoopFee::CoopFee(): Unownable("CoopFee") {}

CoopFee::~CoopFee() {}

void CoopFee::landOn(Human *h) {
  cout << "You landed on pay Coop Fee!" << endl;
  cout << "Charging $150" << endl;
  h->makePayment(150);
}

void CoopFee::landOn(Computer *c) {
  cout << "Computer landed on pay Coop Fee!" << endl;
  cout << "Charging $150" << endl;
  c->makePayment(150);
}
