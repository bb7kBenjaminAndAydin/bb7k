#ifndef __ACADEMICBUILDINGS_H__
#define __ACADEMICBUILDINGS_H__
#include "ownable.h"


class AcademicBuildings: public Ownable {
    int improvementCost;
    int *tuitionPerImprovement;
    int numImprovements;
  public:
    AcademicBuildings(std::string name, std::string academicBlock,int purchaseCose, int improvementCost, int *tuitionPerImprovement);
    ~AcademicBuildings();
    void landOn(Computer *c);
    void landOn(Human *h);
    void mortgage(Player *p);
    void unMortgage(Player *p);
    void improve(Player *p);
    void unImprove(Player *p);
    void returnToBoard();
    void save(std::ofstream &file);
    void getImprovements();
    void changeOwner(Player *p);
    void swapOwner(Player *p);
    void load(Player *p, int improvements);
    bool isImproved();
};

#endif
