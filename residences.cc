#include "residences.h"
#include <iostream>
#include <cstdlib>
using namespace std;

#include "human.h"
#include "computer.h"

Residences::Residences(string name): Ownable(name, "Res", 200){}

Residences::~Residences(){}

void Residences::landOn(Computer *c){
  cout << "You landed on " << this->name << endl;
  if (this->whoOwns==0){
    if (c->getMoney() > this->purchaseCost) {
      c->addBuilding(this);
      this->whoOwns = c;
      c->changeAssets(this->purchaseCost);
      c->makePayment(this->purchaseCost);
    }
  }
  else if (c != this->whoOwns && !(this->mortgaged)){
    int numOwned = 1;
    for (int i =0; i < this->numInBlock; i++){
      if (this->getOwner() == othersInBlock[i]->getOwner()) {
        numOwned++;
      }
    }
    int rentCharged = 25;
    for (int i = 1; i < numOwned; i++) {
      rentCharged *= 2; 
    }
    cout << "Charging: " << rentCharged << endl;
    c->makePayment(rentCharged, this->whoOwns);
  }
  else {}
}

void Residences::landOn(Human *h){
  cout << "You landed on " << this->name << endl;
  if (this->whoOwns==0){
    string likeToPurchase;
    cout << this->name << " is not purchased. Would you like to purchase it? (y/n)" << endl;
    cout << this->name << " costs: " << this->purchaseCost << endl;
    while(cin >> likeToPurchase) {
      if(likeToPurchase == "y" || likeToPurchase == "n") {
        break;
      }
      cout << "Please enter y or n" << endl;
    }
    if (likeToPurchase == "y") {
      h->addBuilding(this);
      this->whoOwns = h;
      h->changeAssets(this->purchaseCost);
      h->makePayment(this->purchaseCost);
    }
  }
  else if (h != this->whoOwns && !(this->mortgaged)){
    int numOwned = 1;
    for (int i =0; i < this->numInBlock; i++){
      if (this->getOwner() == othersInBlock[i]->getOwner()) {
        numOwned++;
      }
    }
    int rentCharged = 25;
    for (int i = 1; i < numOwned; i++) {
      rentCharged *= 2; 
    }
    cout << "Charging: " << rentCharged << endl;
    h->makePayment(rentCharged, this->whoOwns);
  }
  else {}
}

void Residences::mortgage(Player *p){
  if (this->getOwner() != p) {
    cout << "You do not own " << this->name << endl;
  }
  else if (mortgaged) {
    cout << "Already mortgaged" << endl;
  }
  else {
    p->acceptPayment(this->purchaseCost * 0.5);
    mortgaged = true;
  }
}
void Residences::unMortgage(Player *p){
  if (this->getOwner() != p) {
    cout << "You do not own " << this->name << endl;
  }
  else if (mortgaged) {
    p->makePayment((this->purchaseCost * 0.5) + (this->purchaseCost * 0.1));
    mortgaged = false;
  }
  else {
    cout << "Not mortgaged" << endl;
  }
}
void Residences::improve(Player *p) {
  cout << "Cannot improve Residences" << endl;
}
void Residences::unImprove(Player *p) {
  cout << "Cannot unimprove Residences" << endl;
}
void Residences::returnToBoard(){
  this->whoOwns = 0;
  this->mortgaged = false;
}
void Residences::save(ofstream &file) {
  file << this->name;
  if (this->whoOwns == 0) {
    file << " BANK ";
  }
  else {
    file << " " << this->whoOwns->getName() << " ";
  }
  if (this->mortgaged) {
    file << -1;
  }
  else {
    file << 0;
  }
  file << endl;
}

void Residences::getImprovements() {}

bool Residences::isImproved() { return false; }
void Residences::changeOwner(Player *p){
  if (this->whoOwns != 0) {
    this->whoOwns->changeAssets(-(this->purchaseCost));
  }
  if (p != 0) {
    p->changeAssets(this->purchaseCost);
  }
  this->whoOwns = p;
}
void Residences::swapOwner(Player *p){
  if (this->whoOwns != 0) {
    this->whoOwns->changeAssets(-(this->purchaseCost));
  }
  if (p != 0) {
    p->changeAssets(this->purchaseCost);
  }
  this->whoOwns = p;
  if (this->mortgaged) {
    p->makePayment(this->purchaseCost * 0.1);
  }
}

void Residences::load(Player *p, int improvements){
  if (improvements == -1) {
    this->mortgaged = true;
  }
  else {

  }

  this->changeOwner(p);
  p->addBuilding(this);
}
